import { firestore } from '@/firebase'
import { collection, updateDoc, getDocs, addDoc, deleteDoc, doc } from 'firebase/firestore'

export const TASKS = 'tasks'

export const tasksCollection = collection(firestore, TASKS)

export const getTaskData = () => getDocs(tasksCollection)

export const addTaskDoc = (taskModel) => addDoc(tasksCollection, taskModel)

export const updateTaskDataField = (taskRef, field, data) =>
  updateDoc(doc(firestore, TASKS, taskRef), field, data)

export const deleteTaskData = (taskRef) => deleteDoc(doc(firestore, TASKS, taskRef))
