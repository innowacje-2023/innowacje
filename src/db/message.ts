import { firestore } from "@/firebase"
import { getDocs, addDoc, collection } from "firebase/firestore"

export const MESSAGES = 'messages'
export const collectionMessages = collection(firestore,MESSAGES)
export const getMessagesDoc = () => getDocs(collectionMessages)
export const addMessageDoc = (data) => addDoc(collectionMessages,data)