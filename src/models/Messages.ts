export interface IMessage {
    name: string
    description: string
    date: string
}