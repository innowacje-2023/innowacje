export interface ITask {
  id: string
  status: boolean
  value: string
  edit?: boolean
}
