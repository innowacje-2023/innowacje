import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'

import { createI18n } from 'vue-i18n'
import en from './locale/en.json'
import pl from './locale/pl.json'

const vuetify = createVuetify({
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi
    }
  }
})

const i18n = createI18n({
  mode: 'composition',
  locale: 'pl',
  fallbackLocale: 'en',
  legacy: false,
  messages: {
    en: en,
    pl: pl
  }
})

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify)
app.use(i18n)

app.mount('#app')
