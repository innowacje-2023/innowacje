import { initializeApp } from 'firebase/app'
import { getAnalytics } from 'firebase/analytics'
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyCaTyPr_rXsv-FdjaoTm_tch9LT4HNz4Fs',
  authDomain: 'innowacje-2615c.firebaseapp.com',
  projectId: 'innowacje-2615c',
  storageBucket: 'innowacje-2615c.appspot.com',
  messagingSenderId: '643422175189',
  appId: '1:643422175189:web:b039263265a5a79afb075c',
  measurementId: 'G-ES8CM6ZCKD'
}

export const firebase = initializeApp(firebaseConfig)
export const analytics = getAnalytics(firebase)
export const auth = getAuth(firebase)
export const firestore = getFirestore(firebase)
