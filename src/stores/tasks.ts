import { Ref, ref } from 'vue'
import { defineStore } from 'pinia'
import { ITask } from '@/models/Task'
import { addTaskDoc, deleteTaskData, getTaskData, updateTaskDataField } from '@/db/tasks'

export const useTasksStore = defineStore('tasks', () => {
  const tasks: Ref<ITask[]> = ref([])

  const getTasks = () => {
    getTaskData().then(({ docs }) => {
      tasks.value = docs.map((doc) => ({
        id: doc.id,
        status: doc.data().status,
        value: doc.data().value
      }))
    })
  }

  const onCreateTask = (value) => {
    const newTask = {
      status: false,
      value: value
    }
    addTaskDoc(newTask).then((docRef) => {
      tasks.value.push({
        id: docRef.id,
        status: false,
        value: value
      })
    })
  }

  const onChangeStatusTask = (id, value) => {
    updateTaskDataField(id, 'status', !value).then(() => {
      tasks.value = tasks.value.map((item) =>
        item.id === id ? { ...item, status: !item.status } : item
      )
    })
  }

  const onUpdateTask = (id, value) => {
    updateTaskDataField(id, 'value', value).then(() => {
      tasks.value = tasks.value.map((item) => (item.id === id ? { ...item, value: value } : item))
    })
  }

  const onSortTasks = () => {
    tasks.value = tasks.value.sort((a, b) => {
      if (a.value < b.value) {
        return -1
      }
      if (a.value > b.value) {
        return 1
      }
      return 0
    })
  }

  const onDeleteTask = (id) => {
    deleteTaskData(id).then(() => {
      console.log("check2")
      tasks.value = tasks.value.filter((item) => item.id !== id)
    })
  }

  const $reset = () => {
    tasks.value = []
  }

  return {
    tasks,
    getTasks,
    onCreateTask,
    onChangeStatusTask,
    onUpdateTask,
    onDeleteTask,
    onSortTasks,
    $reset
  }
})
