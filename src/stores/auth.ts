import { defineStore } from 'pinia'
import {
  GoogleAuthProvider,
  type UserCredential,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut
} from 'firebase/auth'
import { type Ref, ref } from 'vue'
import router from '@/router'
import { useTasksStore } from './tasks'
import { auth } from '@/firebase'

export const useAuthStore = defineStore('auth', () => {
  const userAuth: Ref = ref()

  const provider = new GoogleAuthProvider()

  const $reset = () => {
    userAuth.value = null
  }

  const onSuccessAuth = async (response: UserCredential) => {
    try {
      userAuth.value = response.user
      router.push('/dashboard')
    } catch (error) {
      console.log(error)
    }
  }

  const onErrorAuth = (error: Error) => {
    console.log(error)
  }

  const signUpUser = (email: string, password: string) => {
    createUserWithEmailAndPassword(auth, email, password).then(onSuccessAuth).catch(onErrorAuth)
  }

  const signUpUserGoogle = () => {
    signInWithPopup(auth, provider).then(onSuccessAuth).catch(onErrorAuth)
  }

  const loginUser = (email: string, password: string) => {
    signInWithEmailAndPassword(auth, email, password).then(onSuccessAuth).catch(onErrorAuth)
  }

  const loginUserGoogle = () => {
    signInWithPopup(auth, provider).then(onSuccessAuth).catch(onErrorAuth)
  }

  const onSuccessCleanData = () => {
    $reset()
    useTasksStore().$reset()
    router.push('/')
  }

  const logoutUser = () => {
    signOut(auth).then(onSuccessCleanData).catch(onErrorAuth)
  }

  const currentUser = () =>
    new Promise((resolve, reject) => {
      const unsuscribe = onAuthStateChanged(
        auth,
        async (user) => {
          if (user) {
            userAuth.value = user
          } else {
            onSuccessCleanData()
          }
          resolve(user)
        },
        (e) => reject(e)
      )
      unsuscribe()
    })

  return {
    userAuth,

    $reset,
    signUpUser,
    signUpUserGoogle,
    loginUser,
    loginUserGoogle,
    logoutUser,
    currentUser
  }
})
