import { IMessage } from "@/models/Messages"
import { addMessageDoc, getMessagesDoc } from "@/db/message"
import { Ref, ref } from "vue"
import { defineStore } from "pinia"

export const messageStore = defineStore(('messages'), () => {
    const message :Ref<IMessage[]> = ref([])

    const getMessages = () => {
        getMessagesDoc().then((data) => {
            data.forEach((modelMessage) => {
                // console.log(message)
                console.log((modelMessage.data()))
                message.value.push(modelMessage.data() as IMessage)               
            }) 
        })
    }
    const addMessage = (messageAdd) => {
        addMessageDoc(messageAdd).then(() => {
            message.value.push(messageAdd.value)
        })
    }
    return { addMessage, getMessages, message}
})
